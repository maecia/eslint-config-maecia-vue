module.exports = {
  parserOptions: {
    ecmaFeatures: {
      impliedStrict: true
    },
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
    es6: true
  },
  extends: ['plugin:vue/recommended', 'prettier', 'prettier/vue'],
  // required to lint *.vue files
  plugins: ['prettier', 'vue'],
  rules: {
    // prettier config
    'prettier/prettier': ['error']
  },
  // check if imports actually resolve
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'vue/require-prop-types': 0
      }
    }
  ]
}